#  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
## Print the output by PrintHepMC
from TruthIO.TruthIOConf import PrintHepMC
topAlg += PrintHepMC()
