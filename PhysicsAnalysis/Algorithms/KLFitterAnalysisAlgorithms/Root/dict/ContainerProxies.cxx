/*
   Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
*/

#include "KLFitterAnalysisAlgorithms/KLFitterResultContainer.h"
#include "xAODCore/AddDVProxy.h"

ADD_NS_DV_PROXY(xAOD, KLFitterResultContainer);
