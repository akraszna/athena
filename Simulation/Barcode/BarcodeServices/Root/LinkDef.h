/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#ifndef __BARCODESERVICES__
#define __BARCODESERVICES__

#include <vector>
#include <string>
#include <map>
#include <TString.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#endif

#endif

